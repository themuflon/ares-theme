// LightDM callbacks

function show_prompt(text)
{
}

function clear_timeout()
{
}


function reset()
{
   $("#password").val("").focus();
}

function authentication_complete()
{
   if (lightdm.is_authenticated)
	   lightdm.login(lightdm.authentication_user, lightdm.default_session);
   else
	   $("#password").animate({ backgroundColor: '#BA3435'}, 0).animate({ backgroundColor: '#313234'}, 1000);

   reset ();
}

// Init

$(document).ready(
	function()
	{
		lightdm.cancel_timed_login();
		
		if(!lightdm.can_suspend) $("#suspend").hide();
		if(!lightdm.can_hibernate) $("#hibernate").hide();
		if(!lightdm.can_restart) $("#restart").hide();
		if(!lightdm.can_shutdown) $("#shutdown").hide();
		
		$("#suspend").click(function() { lightdm.suspend(); });
		$("#hibernate").click(function() { lightdm.hibernate(); });
		$("#restart").click(function() { lightdm.restart(); });
		$("#shutdown").click(function() { lightdm.shutdown(); });
		
		var provideSecret = function()
		{
			var password = $("#password").val();
			lightdm.provide_secret(password);
		}
		
		var login = function(e)
		{
			if(e.keyCode == 13)
			{
				var username = $("#username").val();

				lightdm.start_authentication(username);
				setTimeout(provideSecret, 100);
			}
		}

		$("#password").keypress(login);
		
		var enterPassword = function(e)
		{
			if(e.keyCode == 13)
			{
				$("#password")
					.val("")
					.focus();
			}
		}
		
		$("#username").keypress(enterPassword);
		
		$("#hostname").text(lightdm.hostname);
		$("#username").focus();
	}
);

